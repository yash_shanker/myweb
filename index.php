<?php
// Merchant key here as provided by Payu
//$MERCHANT_KEY = "576gVo"; For payU money
$MERCHANT_KEY = "xLjU9p";

// Merchant Salt as provided by Payu
$SALT = "4V8xHaOU";
//$SALT = "Q16kix7O"; payu money

// End point - change to https://secure.payu.in for LIVE mode
$PAYU_BASE_URL = "https://test.payu.in";

$action = '';

$posted = array();
if(!empty($_POST)) {
    //print_r($_POST);
  foreach($_POST as $key => $value) {
    $posted[$key] = $value;

  }
}

$formError = 0;

if(empty($posted['txnid'])) {
  // Generate random transaction id
  $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
} else {
  $txnid = $posted['txnid'];
}
$hash = '';
// Hash Sequence
$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
//echo $hashSequence;
if(empty($posted['hash']) && sizeof($posted) > 0) {
  if(
          empty($posted['key'])
          || empty($posted['txnid'])
          || empty($posted['amount'])
          || empty($posted['firstname'])
          || empty($posted['email'])
          || empty($posted['phone'])
          || empty($posted['productinfo'])
          || empty($posted['surl'])
          || empty($posted['furl'])

  )
  {
    $formError = 1;
  }
  else
  {
    //$posted['productinfo'] = json_encode(json_decode('[{"name":"tutionfee","description":"","value":"500","isRequired":"false"},{"name":"developmentfee","description":"monthly tution fee","value":"1500","isRequired":"false"}]'));
   $hashVarsSeq = explode('|', $hashSequence);
    $hash_string = '';
   foreach($hashVarsSeq as $hash_var) {
      $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
      $hash_string .= '|';
    }

    $hash_string .= $SALT;


    $hash = strtolower(hash('sha512', $hash_string));
    $action = $PAYU_BASE_URL . '/_payment';
  }
} elseif(!empty($posted['hash'])) {
  $hash = $posted['hash'];
  $action = $PAYU_BASE_URL . '/_payment';
}
?>



<!DOCTYPE html>
<!--[if lt IE 8 ]><html class="no-js ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 8)|!(IE)]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>

   <!-- Latest compiled and minified CSS -->
   <link rel="stylesheet" href="css/bootstrap.min.css">

   <!-- jQuery library -->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

   <!-- Latest compiled JavaScript -->
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

   <!--- Basic Page Needs
   ================================================== -->
   <meta charset="utf-8">
	<title>Yash Shanker Srivastava</title>
	<meta name="description" content="">
	<meta name="author" content="">

   <!-- Mobile Specific Metas
   ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS
    ================================================== -->
   <link rel="stylesheet" href="css/default.css">
	<link rel="stylesheet" href="css/layout.css">
   <link rel="stylesheet" href="css/media-queries.css">
   <link rel="stylesheet" href="css/magnific-popup.css">



   <!-- Script
   ================================================== -->
	<script src="js/modernizr.js"></script>

   <!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="face.ico" >


                     <script>
                        var hash = '<?php echo $hash ?>';
                        function submitPayuForm() {
                           if(hash == '') {
                              return;
                           }
                           var payuForm = document.forms.payuForm;
                           payuForm.submit();
                        }
                     </script>


</head>

<body onload="submitPayuForm()">

   <!-- Header
   ================================================== -->
   <header id="home">

      <nav id="nav-wrap">

         <a class="mobile-btn" href="#nav-wrap" title="Show navigation">Show navigation</a>
	      <a class="mobile-btn" href="#" title="Hide navigation">Hide navigation</a>

         <ul id="nav" class="nav">
            <li class="current"><a class="smoothscroll" href="#home">Home</a></li>
            <li><a class="smoothscroll" href="#about">About</a></li>
	         <li><a class="smoothscroll" href="#education">Education</a></li>
            <li><a class="smoothscroll" href="#projects">Experience</a></li>
            <li><a class="smoothscroll" href="#skill">Skills</a></li>
            <li><a class="smoothscroll" href="#testimonials">Testimonials</a></li>
            <li><a class="smoothscroll" href="#contact">Contact</a></li>
            <li><a class="smoothscroll" href="#hireme">Hire Me</a></li>
	         <li><a target="_blank" href="http://yashkikahaniyan.blogspot.com">Blog</a></li>
         </ul> <!-- end #nav -->

      </nav> <!-- end #nav-wrap -->

      <div class="row banner">
         <div class="banner-text">
            <h1 class="responsive-headline">Yash Shanker Srivastava</h1>
            <h2 style="color:white;<!--#F06000;-->">DevOps Engineer</h2><br>
            <h2 style="color:white;"><span>Cloud | Jenkins | AWS | CI & CD | DevOps | Agile</span></h2>
            <!--
              <h3>I'm a India based budding <span>FullStack Developer</span>, <span>Cloud Computing</span> and <span>Big Data</span> enthusiast.
			Yearning to make a connected world and reduce human efforts by the means of Computer Science. <br><br>
			<a class="smoothscroll" href="#about">Start scrolling and learn more about me</a>.</h3>
            -->
            <hr />
            <ul class="social">
               <li><a target="_blank" href="https://www.facebook.com/yash.bhs"><i class="fa fa-facebook"></i></a></li>
               <li><a target="_blank" href="https://twitter.com/yash_bhs"><i class="fa fa-twitter"></i></a></li>
               <li><a target="_blank" href="https://plus.google.com/u/0/+YashSrivastavaHarHarMahadev/"><i class="fa fa-google-plus"></i></a></li>
               <li><a target="_blank" href="https://in.linkedin.com/in/yash-shanker-srivastava-14677879"><i class="fa fa-linkedin"></i></a></li>
               <li><a target="_blank" href="https://www.instagram.com/yashshanker/"><i class="fa fa-instagram"></i></a></li>
          <!--     <li><a target="_blank" href="#"><i class="fa fa-github"></i></a></li> -->
          <!--     <li><a target="_blank" href="#"><i class="fa fa-skype"></i></a></li>  -->

            </ul>
         </div>
      </div>

      <p class="scrolldown">
         <a class="smoothscroll" href="#about"><i class="icon-down-circle"></i></a>
      </p>

   </header> <!-- Header End -->


   <!-- About Section
   ================================================== -->
   <section id="about">

      <div class="row">

         <div class="three columns">

            <img class="profile-pic"  src="avatar.jpg" alt="" />

         </div>

         <div class="nine columns main-col">

            <h2>About Me</h2>
          <p>I have an experience of over one and a half year, in the field of DevOps.
           In my organization, I am responsible for setting up the DevOps culture from scratch, 
           and then working on its continuous improvement. It mostly includes developing and managing a cost efficient, highly 
           scalable AWS infrastructure for all the applications, and , developing build and release pipelines on Jenkins 
           for Java, PHP, Angular, ReactJS, ReactNative, Native Android and Native iOS; and I have also worked on servicing similar
           DevOps services to a few client teams.
          <br><br>
           I spend most of my free time learning new things. Right now, I am learning Blockchain Development and Solidity. 
          <br><br>
           I love going to Meetups and attending Conferences, to grab knowledge and grow myself and my organization, as well as give 
           back to the community as much as I can.
           <p>
               <div class="columns download">
                  <p>
                     <a target="_blank" href="Yash_Shanker_Resume.pdf" class="button"><i class="fa fa-download"></i>Download Resume</a>
                  </p>
               </div>

            </div> <!-- end row -->

         </div> <!-- end .main-col -->

      </div>

   </section> <!-- About Section End-->



      <!-- Education
      ----------------------------------------------- -->

   <section id="education">
      <div class="row education">

         <div class="three columns header-col">
            <h1><span>Education</span></h1>
         </div>

         <div class="nine columns main-col">

            <div class="row item">

               <div class="twelve columns">

                  <h3>University of Petroleum and Energy Studies</h3>
                  <p class="info">B.Tech. in Computer Science and Engineering with specialization in Cloud Computing and Virtualization
				  Technology in association with IBM
				  <span>&bull;</span> <em class="date">May 2017</em></p>

                  <p>
                  I am currently persuing my Undergraduate Degree from UPES with specialization in CSE-CCVT. Learning about latest technologies, likes of
					IOT, Big Data, Cloud Computing and Virtualization Technologies is the main focus. I was a part of UPES Model United Nations Club
					and held the offices of Joint Head of Events Committee, and Joint Secretary, UPES ACM Student Chapter for sessions 2014-15 and 2015-16
					respectively.
                  </p>

               </div>

            </div> <!-- item end -->

            <div class="row item">

               <div class="twelve columns">

                  <h3>Boys' High School & College, Allahabad.</h3>
                  <p class="info">Secondary and Senior Secondary, CISCE Board<span>&bull;</span> <em class="date">May 2012</em></p>

                  <p>
                  I spent my school life as a day scholar in one of the most prestigious Day Boarding schools in India. Being Alma Mater to many eminent
				  personalities like Amitabh Bachchan, Justice Markandey Katju, etc. the school has groomed me and polished my personality, making me what
				  I am today.
                  </p>

               </div>

            </div> <!-- item end -->

         </div> <!-- main-col end -->

      </div> <!-- End Education -->
   </section>


   <section id="projects">
      <div class="row work">

         <div class="three columns header-col">
            <h1><span>Experience</span></h1>
         </div>

         <div class="nine columns main-col">
            
          

          <div class="row item">

              <div class="twelve columns">

                  <h3>DevOps Engineer</h3>
                  <p class="info">DevOps Engineer: The LeanApps Software Pvt Limited <span>&bull;</span> <em class="date">Feb 2017 - Current</em></p>

                  <p>
                  I was among the ones who set up DevOps practices in the organization from scratch. 
                  Alongwith being responsible for Build and Release automation and setting up CI/CD Pipelines for all applications , 
                  I manage the AWS infrastructure, monitor multiple Production Servers and try to make lives of developers easier 
                  through automation using Python. Apart from spending most of time inside terminals of remote linux servers, 
                  monitoring and tweaking, I am also responsible for researching new technology and implementing them in the applications 
                  after a thorough POC.
                  </p>

              </div>

          </div> <!-- item end -->  

    
            
           <div class="row item">

                    <div class="twelve columns">

                       <h3>E-Mail Invoice and Bulk Upload Customization for Home Credit and Flextronics [LIVE]</h3>
                       <p class="info">Software Development Intern : PayU Payments India Ltd <span>&bull;</span> <em class="date">August 2016</em></p>

                       <p>
                       In this Project, we customized the e-mail invoice and Bulk Upload feature of PayUBiz for merchants HomeCredit and Flextronics.
                       </p>

                    </div>

                 </div> <!-- item end -->            
            
           <div class="row item">

                    <div class="twelve columns">

                       <h3>Development of a WebFront Customization Platform for Merchants of PayUBiz</h3>
                       <p class="info">Software Development Intern : PayU Payments India Ltd <span>&bull;</span> <em class="date">June 2016 - July 2016</em></p>

                       <p>
                       In this Project, we created a platform for the PayUBiz merchant to give him a feature to customize his entire WebFront, i.e., the Header, Footer and Body according to his requirements just by drag and drop method. Merchants will little or even no knowledge of website development can Create and Customize their WebFronts and deploy them for their customers. <br><br>

                      We integrated a Merchant Panel for the Merchant to Create, Edit, Delete and View his WebFronts and a SuperUser panel for the PayU Integration team to manage all the Merchants and all their WebFronts.
                       </p>

                    </div>

                 </div> <!-- item end -->


           <div class="row item">

                    <div class="twelve columns">

                       <h3>Development of Training Module for PayU Biz [Live on PayU Internal Server]</h3>
                       <p class="info">Software Development Intern : PayU Payments India Ltd <span>&bull;</span> <em class="date">May 2016 - June 2016</em></p>

                       <p>
                       In this Project we created a Training Module for a new joinee who joins the PayU Biz Teams, to understand the workflow and mechanisms of all transactions that can be processed through PayUBiz and PayUmoney. The training module gives the trainees a hands on experience on all the PayUBiz APIs though which they can make payments on the test server and see the responses. <br><br>

                      We also integrated a Quiz Module to facilitate the Reporting Managers or Team Leads to test the Trainees and keep a track of their progress. In addition to the Quiz Module, we integrated the Admin Panel through which Reporting Managers and Team Leads can add more Admins the panel, reset passwords and Authentication Code for Quiz, and keep a track on trainees' progress.
                       </p>

                    </div>

                 </div> <!-- item end -->

                        <!--
           <div class="row item">

                    <div class="twelve columns">

                       <h3>Design and Implementation of Resource Allocation Algorithm</h3>
                       <p class="info">Cloud Implementer <span>&bull;</span> <em class="date">February 2016 - April 2016</em></p>

                       <p>
                       Design and Implementation of a dynamic priority based resource allocation algorithm using CloudSim.
                       </p>

                    </div>

                 </div> 

			<div class="row item">

               <div class="twelve columns">

                  <h3>Low Power NAS Storage System</h3>
                  <p class="info">Programmer <span>&bull;</span> <em class="date">February 2016 - April 2016</em></p>

                  <p>
                  Developing a Network Attached Storage System using Raspberry Pi 2 and USB Hard Disk Storage.
                  </p>

               </div>

            </div> 


            <div class="row item">

                     <div class="twelve columns">

                        <h3>Network Traffic Simulation using NS2</h3>
                        <p class="info">Network Administrator <span>&bull;</span> <em class="date">February 2016</em></p>

                        <p>
                        Implemented AODV, DSDV and DSR protocols on NS2 and NAM animator and analysed trace file and traffic file.
                        </p>

                     </div>

                  </div> 

			<div class="row item">

               <div class="twelve columns">

                  <h3>Database Management System Using C</h3>
                  <p class="info">Lead Programmer <span>&bull;</span> <em class="date">August 2015 - November 2015</em></p>

                  <p>
                  Developing a Database Management System using the C Programming Language which runs database queries of create,
                  insert, delete, update, etc
                  </p>

               </div>

            </div> 

            <div class="row item">

               <div class="twelve columns">

                  <h3>Server log analysis using Hadoop</h3>
                  <p class="info">Big Data Analyst <span>&bull;</span> <em class="date">May 2015 - July 2015</em></p>

                  <p>
                  Aiming to bring out some value from server log files in the form of stats
				  and future predictions using Apache Hadoop and Map-Reduce.
                  </p>

               </div>

            </div> 



            <div class="row item">

               <div class="twelve columns">

                  <h3>Hostel Mess Food Menu</h3>
                  <p class="info">Android Developer<span>&bull;</span> <em class="date">January 2015 - March 2015</em></p>

                  <p>
                  Developing an Android application to show daily food menus of various hostels.
                  </p>

               </div>

            </div> 
            
            -->



         </div> <!-- main-col end -->

      </div> <!-- End Work -->
   </section>


      <!-- Skills
      ----------------------------------------------- -->
   <section id="skill">   
      <div class="row skill">

         <div class="three columns header-col">
            <h1><span>Skills</span></h1>
         </div>

         <div class="nine columns main-col">

            <p>
            </p>

				<div class="bars">

					<!-- layout.css LineNumber 328 -->

				   <ul class="skills">


						<li><span class="bar-expand aws"></span><em>AWS</em></li>
						<li><span class="bar-expand shellAndPython"></span><em>Shell & Python Scripting </em></li>
						<li><span class="bar-expand jenkins"></span><em>Jenkins</em></li>
						<li><span class="bar-expand cicd"></span><em>CI & CD Best Practices</em></li>
						<li><span class="bar-expand agile"></span><em>Agile</em></li>
						<li><span class="bar-expand docker"></span><em>Docker</em></li>
						<li><span class="bar-expand linux"></span><em>Linux</em></li>
						<li><span class="bar-expand ccvt"></span><em>Cloud Computing and Virtualization Technologies</em></li>
						<li><span class="bar-expand teamplay"></span><em>Teamplay</em></li>
						<li><span class="bar-expand creative"></span><em>Creative</em></li>
					</ul>

				</div><!-- end skill-bars -->

			</div> <!-- main-col end -->

      </div> <!-- End skills -->

   </section> <!-- Resume Section End-->

   <!-- Testimonials Section
   ==================================================
   <section id="testimonials">

      <div class="text-container">

         <div class="row">

            <div class="two columns header-col">

               <h1><span>Client Testimonials</span></h1>

            </div>

            <div class="ten columns flex-container">

               <div class="flexslider">

                  <ul class="slides">

                     <li>
                        <blockquote>
                           <p>Your work is going to fill a large part of your life, and the only way to be truly satisfied is
                           to do what you believe is great work. And the only way to do great work is to love what you do.
                           If you haven't found it yet, keep looking. Don't settle. As with all matters of the heart, you'll know when you find it.
                           </p>
                           <cite>Steve Jobs</cite>
                        </blockquote>
                     </li> <!-- slide ends

                     <li>
                        <blockquote>
                           <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.
                           Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem
                           nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris.
                           </p>
                           <cite>Mr. Adobe</cite>
                        </blockquote>
                     </li> <!-- slide ends

                  </ul>

               </div> <!-- div.flexslider ends

            </div> <!-- div.flex-container ends

         </div> <!-- row ends

       </div>  <!-- text-container ends

   </section> <!-- Testimonials Section End-->



   <!-- Contact Section
   ================================================== -->
   <section id="contact">

         <div class="row section-head">

            <div class="two columns header-col">

               <h1><span>Get In Touch.</span></h1>

            </div>
            <div class="ten columns">

                  <p class="lead">I will be more than glad to hear from you. Drop in your message below.
                  </p>

            </div>

         </div>

         <div class="row">

            <div class="eight columns">

               <!-- form -->
               <form action="mail.php" method="POST" >
					<fieldset>
                  
                  <input type="hidden" value="contact_form" name="do">

                  <table cellspacing="5" style="width:85%;">
                     <tr>
   						   <td><input type="text" value="" size="20" name="name" placeholder="Enter Name" required></td>
                        <td><input type="email" value="" size="20" name="email" placeholder="Enter Email ID" required></td>
                     </tr>

                     <tr>
						      <td><input type="text" value="" size="10" name="phone" placeholder="Enter Phone Number" required></td>
                        <td><input type="text" value="" size="25" name="subject" placeholder="Enter Subject" required></td>
                     </tr>

                     <tr>
                        <td colspan="2">
                           <textarea style="min-width: 89%;" cols="15" rows="5" name="message" placeholder="Enter Your Message" required></textarea>
                        </td>
                     </tr>
                  </table>

                  <div>
                     <button class="submit">Send Message</button>
                     <span id="image-loader">
                        <img alt="" src="images/loader.gif">
                     </span>
                  </div>

					</fieldset>
				   </form> <!-- Form End -->

               <!-- contact-warning -->
               <div id="message-warning"> Error boy</div>
               <!-- contact-success -->
				   <div id="message-success">
                  <i class="fa fa-check"></i>Your message was sent, thank you!<br>
				   </div>

            </div>

            <aside class="four columns footer-widgets">

               <div class="widget widget_contact">

					   <h4>Address and Phone</h4>
					   <p class="address">
						   Yash Shanker Srivastava<br>
						   B-114, The Lean Apps Software Pvt Ltd<br>
						   Kaspate Wasti, Wakad<br>
						   Pune - 411057. Maharashtra.<br>
						   India.<br>
							<span>(+91)  9452939839</span>
							<span>yash.shanker@outlook.com</span><br>
							<span>yashshanker.com</span><br>
					   </p>

				   </div>



            </aside>

      </div>

   </section> <!-- Contact Section End-->



   <!-- Hire Me Section
   ================================================== -->
   <section id="hireme">

         <div class="row section-head">

            <div class="twelve columns" style="margin-top:-3%;">

                  <p class="lead">Let's get in touch, and begint the journey.
                  </p>

            </div>

         </div>

       
         <div class="row">

            <div class="eight columns">

               <div class="container" style="margin-top:-7%; width:100%;">
  
                 <ul class="nav nav-tabs" style="width:100%">
                   <li class="active"><a data-toggle="tab" href="#menu1">Describe Project</a></li>
                   <!-- <li><a data-toggle="tab" href="#menu2">Upload Project File</a></li> -->
                   <li><a data-toggle="tab" href="#menu3">Hire Me as a Regular Employee</a></li>
                 </ul>

                 <div class="tab-content">
                   
                   <div id="menu1" class="tab-pane fade in active">

                     <form action="mail.php" method="post" id="a">
               <fieldset>
                  <br>

                  <input type="hidden" value="describe_project" name="do">

                  <div>
                     <input type="text" value="" size="15"  name="name" placeholder="Enter your Name" required >
                  </div>

                  <div>
                     <input type="text" value="" size="15"  name="email" placeholder="Enter your Email-ID" required>
                  </div>

                  <div>
                     <textarea cols="50" rows="5"   name="description" form="a" placeholder="Enter your Project Description" required></textarea>
                  </div>

                  <div align="center" style="width:100%;">
                     <button class="submit">Submit</button>
                     <span id="image-loader">
                        <img alt="" src="images/loader.gif">
                     </span>
                  </div>

               </fieldset>
               </form>
                     
                   </div>

   <!--                <div id="menu2" class="tab-pane fade">

                      <form action="mail.php" method="post">
               <fieldset>
                  <br>

                  <input type="hidden" value="upload_project_file" name="do">

                  <div>
                     <input type="text" value="" size="15" name="name" placeholder="Enter your Name" required >
                  </div>

                  <div>
                     <input type="email" value="" size="15"  name="email" placeholder="Enter your Email-ID" required>
                  </div>

                  <div>
                     <textarea cols="50" rows="5" name="attachment" placeholder="Enter your Project Description" required></textarea>
                  </div>

                  <div align="center">
                     <button class="submit" >Submit</button>
                     <span id="image-loader">
                        <img alt="" src="images/loader.gif">
                     </span>
                  </div>

               </fieldset>
               </form>
                     
                   </div>
      -->
                  <div id="menu3" class="tab-pane fade">

                     <form action="mail.php" method="post">
               <fieldset>
                  <br>

                  <input type="hidden" value="hire_me_as_regular" name="do">

                  <p>Thank you for your kind consideration. Kindly send your details through the form below and I'll get back to you ASAP.</p>
                  <div>
                     <input type="text"  size="15" name="name" placeholder="Enter your Name"  required>
                  </div>

                  <div>
                     <input type="text"  size="10" name="phone" placeholder="Enter your Contact Number"  required>
                  </div>

                  <div>
                     <input type="email"  size="15" name="email" placeholder="Enter your Email-ID"  required>
                  </div>

                  <div>
                     <input type="text"  size="15" name="company" placeholder="Enter your Organization's Name"  required>
                  </div>

                  

                  <div align="center" style="width:100%;">
                     <button class="submit">Submit</button>
                     <span id="image-loader">
                        <img alt="" src="images/loader.gif">
                     </span>
                  </div>

               </fieldset>
               </form>
                     
                   </div>

                 </div>
               </div>


               <!-- contact-warning -->
               <div id="message-warning"> Error boy</div>
               <!-- contact-success -->
               <div id="message-success">
                  <i class="fa fa-check"></i>Your message was sent, thank you!<br>
               </div>

            </div>

             
            <aside class="four verticalLine columns footer-widgets">

               <div class="widget  widget_contact" style="margin-top:-3%;">

                  <div  align="center"><h4>Pay for your Project</h4></div>





                    

                  <?php if($formError) { ?>
                     <span style="color:red">Please fill all mandatory fields.</span>
                     <br/>
                     <br/>
                   <?php } ?>


                  <form  action="<?php echo $action; ?>" method="post" name="payuForm">
               <fieldset>
                  <br>

                  <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
                  <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
                  <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />
                  
                  <input type="hidden" name="surl" value="<?php echo 'http://'.$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']); ?>/success.php" />
                  <input type="hidden" name="furl" value="<?php echo 'http://'.$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']); ?>/failure.php" />
                  
                  <!-- 
                  <input type="hidden" name="surl" value="www.google.com" />
                  <input type="hidden" name="furl" value="www.facebook.com" />
                  -->

                  <input type="hidden" name="lastname" id="lastname" value="<?php echo (empty($posted['lastname'])) ? '' : $posted['lastname']; ?>" />
                  <input type="hidden" name="curl" value="" />
                  <input type="hidden" name="address1" value="<?php echo (empty($posted['address1'])) ? '' : $posted['address1']; ?>" />
                  <input type="hidden" name="address2" value="<?php echo (empty($posted['address2'])) ? '' : $posted['address2']; ?>" />
                  <input type="hidden" name="city" value="<?php echo (empty($posted['city'])) ? '' : $posted['city']; ?>" />
                  <input type="hidden" name="state" value="<?php echo (empty($posted['state'])) ? '' : $posted['state']; ?>" />
                  <input type="hidden" name="country" value="<?php echo (empty($posted['country'])) ? '' : $posted['country']; ?>" />
                  <input type="hidden" name="zipcode" value="<?php echo (empty($posted['zipcode'])) ? '' : $posted['zipcode']; ?>" />
                  <input type="hidden" name="udf1" value="<?php echo (empty($posted['udf1'])) ? '' : $posted['udf1']; ?>" />
                  <input type="hidden" name="udf2" value="<?php echo (empty($posted['udf2'])) ? '' : $posted['udf2']; ?>" />
                  <input type="hidden" name="udf3" value="<?php echo (empty($posted['udf3'])) ? '' : $posted['udf3']; ?>" />
                  <input type="hidden" name="udf4" value="<?php echo (empty($posted['udf4'])) ? '' : $posted['udf4']; ?>" />
                  <input type="hidden" name="udf5" value="<?php echo (empty($posted['udf5'])) ? '' : $posted['udf5']; ?>" />
                  <input type="hidden" name="pg" value="<?php echo (empty($posted['pg'])) ? '' : $posted['pg']; ?>" />
               
                  <div>
                     <input type="text" size="35" name="firstname" id="firstname" value="<?php echo (empty($posted['firstname'])) ? '' : $posted['firstname']; ?>" placeholder="Enter your Name" style="width:100%;" required>
                  </div>

                  <div>
                     <input type="email" size="35" name="email" id="email" value="<?php echo (empty($posted['email'])) ? '' : $posted['email']; ?>" placeholder="Enter your Email-ID" style="width:100%;" required>
                  </div>

                  <div>
                     <input type="text" size="35" name="phone" value="<?php echo (empty($posted['phone'])) ? '' : $posted['phone']; ?>" placeholder="Enter your Contact Number" style="width:100%;" required>
                  </div>

                  <div>
                     <input type="text" size="35" name="productinfo" value="<?php echo (empty($posted['productinfo'])) ? '' : $posted['productinfo'] ?>" placeholder="Enter your Project Name" style="width:100%;" required>
                  </div>

                  <div>
                     <input type="text" size="35" name="amount" value="<?php echo (empty($posted['amount'])) ? '' : $posted['amount'] ?>" placeholder="Enter Amount" style="width:100%;" required>
                  </div>

                  
<br>
                  <div align="center">

                     <?php if(!$hash) { ?>
                     <button  class="submit" type="submit" value="submit">Pay Now</button>

                     <!--  <button class="submit">Pay Now</button>   
                     <td colspan="4"><input type="submit" value="Submit" /></td> --> 

                     <?php  } ?>

                     <span id="image-loader">
                        <img alt="" src="images/loader.gif">
                     </span>
                  </div>

               </fieldset>
               </form>

               </div>


            </aside>


      </div>

   </section> <!-- Hire Me Section End-->


   <!-- footer
   ================================================== -->
   <footer>

      <div class="row">

         <div class="twelve columns">

            <ul class="social-links">
               <li><a target="_blank" href="https://www.facebook.com/yash.bhs"><i class="fa fa-facebook"></i></a></li>
               <li><a target="_blank" href="https://twitter.com/yash_bhs"><i class="fa fa-twitter"></i></a></li>
               <li><a target="_blank" href="https://plus.google.com/u/0/+YashSrivastavaHarHarMahadev/"><i class="fa fa-google-plus"></i></a></li>
               <li><a target="_blank" href="https://in.linkedin.com/in/yash-shanker-srivastava-14677879"><i class="fa fa-linkedin"></i></a></li>
               <li><a target="_blank" href="https://www.instagram.com/yashshanker/"><i class="fa fa-instagram"></i></a></li>
            <!--   <li><a target="_blank" href="#"><i class="fa fa-github"></i></a></li> -->
            <!--   <li><a target="_blank" href="#"><i class="fa fa-skype"></i></a></li>  -->
            </ul>

            <ul class="copyright">
               <li>Thank You for Visiting</li>
            </ul>

         </div>

         <div id="go-top"><a class="smoothscroll" title="Back to Top" href="#home"><i class="icon-up-open"></i></a></div>

      </div>

   </footer> <!-- Footer End-->

   <!-- Java Script
   ================================================== -->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
   <script>window.jQuery || document.write('<script src="js/jquery-1.10.2.min.js"><\/script>')</script>
   <script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>

   <script src="js/jquery.flexslider.js"></script>
   <script src="js/waypoints.js"></script>
   <script src="js/jquery.fittext.js"></script>
   <script src="js/magnific-popup.js"></script>
   <script src="js/init.js"></script>

</body>

</html>
